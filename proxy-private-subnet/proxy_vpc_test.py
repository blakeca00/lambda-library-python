""" Lambda proxy test (python3)

Trigger:
    Test Event, any contents
Args:
    None
Returns:
    Execution Success | Failure, TYPE:  bool

"""
import os
import urllib.request

redhat = 'http://redhat.com'
pypi = 'https://pypi.python.org'
google = 'http://www.google.com'


def lambda_handler(event, context):
    """ attempts connectivity via proxy, lambda inside vpc """
    # logger.info('Event: ' + str(event))
    # print('Received event: ' + json.dumps(event, indent=2))

    # configure proxy settings for AML
    proxy_ip = '156.150.41.93:3128'
    metadata_ip = '169.254.169.254'

    os.environ['HTTP_PROXY'] = 'http://' + proxy_ip
    os.environ['HTTPS_PROXY'] = 'http://' + proxy_ip
    os.environ['NO_PROXY'] = metadata_ip

    try:
        print('redhat test')
        with urllib.request.urlopen('http://redhat.com/') as response:
            html = response.read()
            print(html)

        print('Running all tests:')
        for url in (redhat, pypi, google):
            with urllib.request.urlopen(url) as response:
                html = response.read()
                print('\n-----------  RESPONSE TO REQUEST URL: %s  --------------' % url)
                print(html)
    except Exception as e:
        print(str(e))
        return False
    return True
