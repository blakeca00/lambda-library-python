* * *
#  Lambda Function Library -- Python
* * *

## Contents

This repo is a library of [python](https://www.python.org) functions for use via [Amazon Web Services' Lambda](http://aws.amazonaws.com/lambda) Service.  Each directory encapsulates all of the required contents for a separate function.  When possible, each directory will contain a function-specific README with the following sections:

1. Summary
2. Source
3. Contents
4. Instructions
5. IAM Permissions

* * *

**Important**: Some lambda functions have been added as git submodules to this repo; as such,
contain links to another repo. When cloning this repo, please do the following:
```bash
$ git clone --recursive https://blakeca00@bitbucket.org/blakeca00/lambda-library-python.git
```
Failure to add the --recursive option will mean some subdirs of this repo appear empty
when cloned to your local machine.

* * *

## Author & Copyright

All works contained herein copyrighted via below author unless work is explicitly noted by an alternate author.

* Copyright Blake Huber, All Rights Reserved.

* * *

## License

* Software contained in this repo is licensed under the [license agreement](https://bitbucket.org/blakeca00/lambda-library-python/src/master/LICENSE.md).

* * *

## Disclaimer

*Code is provided "as is". No liability is assumed by either the code's originating author nor this repo's owner for their use at AWS or any other facility. Furthermore, running function code at AWS may incur monetary charges; in some cases, charges may be substantial. Charges are the sole responsibility of the account holder executing code obtained from this library.*

Additional terms may be found in the complete [license agreement](https://bitbucket.org/blakeca00/lambda-library-python/src/master/LICENSE.md).

* * *
