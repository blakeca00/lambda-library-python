* * * 
# README - aws-snapshot-check
* * *

# Summary

function identifies EC2 volumes and then locates the volume(s) without corresponding snapshot(s). If volumes lacking snapshot backups are detected, it checks if the age of the volume. Based on Onur Salk's aws-snapshot-check-tool.

* * *
