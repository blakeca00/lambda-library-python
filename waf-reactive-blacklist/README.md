***
# Rate-Based Blacklisting with AWS WAF and AWS Lambda
***

## 1. Summary

A solution that automatically detects unwanted requests based on request rate, and then updates configurations of AWS WAF (a web application firewall that protects any application deployed on Amazon CloudFront content delivery service) to block subsequent requests from those users. This process is executed by a lambda function that processes application’s access log files in order to identify bad requesters. This function also exposes execution metrics in CloudWatch so you can monitor how many request entries were processed and the number of origins blocked. Finally, the solution also support that you manually add IP ranges that you want to block in advance like well know bot networks.

***

## 2. Source

AWS Security Blog.  [How to Configure Rate-Based Blacklisting with AWS WAF and AWS Lambda](https://aws.amazon.com/blogs/security/how-to-configure-rate-based-blacklisting-with-aws-waf-and-aws-lambda) by Heitor Vital.

Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Amazon Software License (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at http://aws.amazon.com/asl.

This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied. See the License for the specific language governing permissions and limitations under the License.

***

## 3. Contents

https://github.com/awslabs/aws-waf-sample/tree/master/waf-reactive-blacklist

***

## 4. Instructions

In order to change solution parameters of function, such as how long to block IPs or the Request Rate Threshold, simply update the running cloudformation stack.  
For more instructions, *see section (2) Source above.*

***

## 5. IAM Permissions

*see section (2) Source above.*



