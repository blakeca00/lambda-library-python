#!/usr/bin/env bash
#
# Summary:
#    Triggers a lambda function with a test event payload in multiple regions
#

regions=('eu-west-1' 'us-east-1' 'ap-southeast-1')
function_name='CFN-MPC-AutoTag'
profile='gcreds-phht-gen-ra1-pr'
stdout=''
for region in regions:
    aws --profile $profile lambda invoke \
        --function-name $function_name \
        --payload file://run_instances.json \
        outfile.txt
done
