#!/usr/bin/env python3

"""
Summary:
    Triggers a lambda function with a test event payload in multiple region

Args:
"""

import boto3
import os
import json

session_ra1 = boto3.Session(profile_name='gcreds-phht-gen-ra1-pr', region_name='eu-west-1')

ec2_client = session_ra1.client('ec2')
r = ec2_client.describe_instances()

instances_ra1 = []
with open('instances_ra1.list', 'a') as f1:
    for z in [y for y in [x['Instances'] for x in r['Reservations']]]:
        for i in z:
            instances_ra1.append(i['InstanceId'])
            f1.write(str(i['InstanceId']) + '\n')
    f1.close()
