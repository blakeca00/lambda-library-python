from _version import __version__ as version

__author__ = 'Blake Huber'
__version__ = version
__credits__ = []
__license__ = "MIT"
__maintainer__ = "Blake Huber"
__email__ = "blakeca00[AT]gmail.com"
__status__ = "Development"
