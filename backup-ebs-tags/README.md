* * *
## Serverless Auto-snapshotting based on tag value 
* * * 

## Summary

This lambda function uses tag values as a trigger to take snapshots of ebs volumes in a region.  Function looks for values such as "backup" tagged to the volume. Schedule-driven execution (hourly).

Lambda Function leverages AWS Lambda, EBS, and SNS for notification.

* * * 

## Source

* Author:	pprakash
* Source:	https://pprakash.me/tech/2016/06/09/automated-ebs-snapshots/

## Contents 

* README.md: 	this file 
* policy.json:  IAM role policy for function execution privileges 
* function.py:  lambda function
* Handler:  	lambda_handler

## Sample Tag

```json
{
	"time": {
		"mon": 23,
		"tue": 23,
		"wed": 23,
		"thu": 23,
		"fri": 23,
		"sat": 23,
		"sun": 23
	},
	"retention": 1
}
```
## Instructions

This function contains a config section which defines how you expect the script to behave. If auto-create-tag is set to true the script will check all EC2 instances for a tag named backup, if it exists it will use the value of it. If it doesn’t exists, it will create the tag with the value as defined in default section of config. Script can be configured to ignore certain EC2 instances by specifying their instance ID’s in exclude or the name of the instance in exclude_name section. If an EC2 instance contains a tag named ignore the script will ignore that instance too.

If auto-create-tag is set to false the script will check all EC2 instances for a tag named backup, if it exists it will trigger backup, else ignore that instance.

Once the script identifies that the instance needs to be backed up, it will get the list of EBS volumes attached to that instance and create snapshot of them. Once snapshot creation has been triggered, it will check the number of snapshots created from that volume and delete the older snapshots to maintain the retention period configured in the tags.

If sns_topic has been configured with ARN of the SNS topic, it will send an email notification whenever it fails to create the snapshot.
