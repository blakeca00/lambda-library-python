#  pdf_binary_application | AWS Lambda
* * * 

## Author / License

- Blake Huber
- GPL v3

* * *

## Purpose

Print Objects to PDF File Output from [AWS Lambda](https://aws.amazon.com/lambda)

When executed from Lambda:

* Downloads pre-compiled binary from s3
* Original source binary from [https://wkhtmltopdf.org](https://wkhtmltopdf.org)
* Performs file integrity via hash
* Unpacks binary
* Configures resulting binary and environment PATH variable for use
* Runs test to convert a page to pdf

* * *

## Dependencies

* may need increased memory allocation, depending upon the size of the object output  
to pdf format

* * *

## Contents

### [pdf_binary_application.py](./pdf_binary_application.py)

* for use with python3 runtime

* * * 

