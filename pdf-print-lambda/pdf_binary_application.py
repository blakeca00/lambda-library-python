#!/usr/bin/env python3
from __init__ import __version__
import os
import hashlib
import subprocess
import urllib.request
from urllib import parse
import urllib.error
import boto3
import loggers

logger = loggers.getLogger(__version__)


def pdf_binary_application(binary_url, bin_file, md5_sig):
    """
    Retrieve and install binary application for use (wkhtmltopdf)

        - binary_url : either http url or s3 path (s3://<prefix>/bin_file)
        - bin_file : application binary object to retrieve
        - md5_sig : file object containing 1 or more md5 hashes in .md5 format

    """
    def _check_md5(file, hash_file):
        """ validate md5 hash, return False if integrity check fail """
        bits = 4096
        # calc md5 hash
        hash_md5 = hashlib.md5()
        with open(file, "rb") as f:
            for chunk in iter(lambda: f.read(bits), b""):
                hash_md5.update(chunk)
        # locate hash signature for file, validate
        with open(hash_file) as c:
            for line in c.readlines():
                if line.strip():
                    check_list = line.split()
                    if file == check_list[1]:
                        if check_list[0] == hash_md5.hexdigest():
                            return True
                        else:
                            return False
    # static vars
    binary_url = '/'.join(binary_url.split('/')[:-1])    # remove object keyname
    tmp_dir = '/tmp'
    bin_tar = '.'.join(bin_file.split('.')[:-1])
    bin_final = '/wkhtmltox/bin'

    # object retrieval
    os.chdir(tmp_dir)
    logger.info('begin download of required s3 objects')

    try:
        s3_objects = [bin_file, md5_sig]
        if 'http' in binary_url:
            # http/s
            for file_obj in s3_objects:
                urllib.request.urlretrieve(
                    binary_url + '/' + file_obj,
                    tmp_dir + '/' + file_obj
                )
                logger.info(file_obj + ' downloaded successfully')
        else:
            # s3://url, boto sdk
            S3_CLIENT = boto3.client('s3')
            s3_bucket = binary_url.split('/')[2]
            s3_key = '/'.join(binary_url.split('/')[3:])
            for file_obj in s3_objects:
                S3_CLIENT.download_file(
                    s3_bucket,
                    s3_key + '/' + file_obj,
                    file_obj
                )
                logger.info(file_obj + ' downloaded successfully')

    except urllib.error.HTTPError as e:
        logger.critical('failed calling url %s. Exception: %s, data: %s' % (binary_url, e, e.read()))
        return {'GENERATE_PDF': False}
    except ClientError as e:
        logger.critical("Error in control (%s) (Code: %s Message: %s)" %
            (control, e.response['Error']['Code'], e.response['Error']['Message']))
    except Exception as e:
        logger.critical('failed calling url %s. Exception: %s' % (binary_url, e))
        return {'GENERATE_PDF': False}

    # validate file integrity
    check_result = _check_md5(file=bin_file, hash_file=md5_sig)
    if check_result == False:
        logger.warning('md5 checksum does not match reference signature. Integrity fail \
                on object retrieved from s3' + bin_file)
        return {'GENERATE_PDF': False}
    else:
        #  binary installation & configuration
        try:
            # bash host commands
            bash_cmds = [
                'unxz ' + tmp_dir + '/' + bin_file,
                'tar -xvf ' + tmp_dir + '/' + bin_tar,
            ]
            for cmd in bash_cmds:
                process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
                output, error = process.communicate()
                logger.info('executing bash command: ' + cmd)
                for line in output.splitlines():
                    logger.info(line)
            # prep binary for use
            os.chmod(tmp_dir + bin_final, 0o777)
            os.environ['PATH'] = str(os.environ['PATH']) + ':' + tmp_dir + bin_final

        except subprocess.CalledProcessError as e:
            logger.warning('bash command (%s) exit code %s. Exception: %s' % (e.cmd, e.returncode, e))
            return {'GENERATE_PDF': False}
        except FileNotFoundError as e:
            logger.warning('bash command (%s) failed. Exception: %s' % (cmd, e))
            return {'GENERATE_PDF': False}
        except Exception as e:
            logger.critical('Unknown Exception while executing file operations: %s' % (e))
            return {'GENERATE_PDF': False}
    return {'GENERATE_PDF': True}


def lambda_handler(event, context):
    """ Test pdf generation """

    output_file = event['output_file']
    bucket = event['output_bucket']
    url = event['source_url']
    s3_url = event['binary_source_url']

    response = pdf_binary_application(
                binary_url=s3_url,
                bin_file='wkhtmltox-0.12.4_linux-generic-amd64.tar.xz',
                md5_sig = 'MD5SUMS.md5'
            )
    # abort pdf output if installation / configuration failure
    if response['GENERATE_PDF'] == False:
        PDF_FLAG = False
        loger.warning('pdf binary application setup failure. PDF report format \
                    will not generate. PDF_FLAG set to ' + str(PDF_FLAG)
        )
    else:
        # normally, we use pdfkit python wrapper for wkhtmltopdf here, but are
        # simply executing the binary directly using a local bash command to
        # prevent having to include pdfkit as a dep module

        bash_cmds = ['wkhtmltopdf ' + url + ' /tmp/' + output_file]
        for cmd in bash_cmds:
            process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
            output, error = process.communicate()
            logger.info('executing bash command: ' + cmd)
            for line in output.splitlines():
                logger.info(line)

        # upload to s3
        S3_CLIENT = boto3.client('s3')
        S3_CLIENT.upload_file('/tmp/' + output_file, bucket, output_file)
    return 0
