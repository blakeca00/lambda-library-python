"""
Configuration File for class BinaryApplication
    - dict object for each type of binary application

"""

""" pdf printing based on wkhtmltopdf binary """

binary_config = [
    {
        'binary': 'wkhtmltopdf',
        'awsRegion': 'eu-west-1',
        'Configuration': {
            'filename': 'wkhtmltox-0.12.4_linux-generic-amd64.tar.xz',
            'url': 's3://eu-west-1-mpc-install-pr/Code/pdf-lambda'
            'md5_sig': 'MD5SUMS.md5',
            'bin_final': '/wkhtmltox/bin'
        }
    },
    {
        'binary': 'something',
        'awsRegion': 'us-east-1',
        'Configuration': {
            'filename': 'wkhtmltox-0.12.4_linux-generic-amd64.tar.xz',
            'url': 's3://us-east-1-mpc-install-pr/Code/something'
            'md5_sig': 'MD5SUMS.md5',
            'bin_final': '/somefile.bin'
        }
    }
]
